#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>

product convert(char* str)
{
    product result;
    char* context = NULL;
    char* str_number = strtok_s(str, "    ", &context);
    result.price = atof(str_number);
    str_number = strtok_s(NULL, "    ", &context);
    result.quantity = atoi(str_number);
    return result;
}

void read(const char* file_name, product_catalog** array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            product_catalog** item = new product_catalog*;
            file >> item->product.category;
            file >> item->product.product_name;
            file >> tmp_buffer;
            item->product = convert(tmp_buffer);
           
            file.read(tmp_buffer, 1); // ������ ������� ������� �������
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}