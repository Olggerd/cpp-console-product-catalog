#include <iostream>
#include <iomanip>

using namespace std;

#include "product-catalog.h"
#include "file_reader.h"
#include "constants.h"

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �4. GIT\n";
    cout << "������� �10. ������� �������\n";
    cout << "�����: ���� �������\n\n";
    product_catalog* catalog[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", catalog, size);
        cout << "***** ������� ������� *****\n\n";
        for (int i = 0; i < size; i++)
        {
            /********** ����� ��������� **********/
            cout << "��������� ������........: ";
          
            cout << catalog[i]->product.category << '\n';
           
          
            /********** ����� �������� **********/
            cout << "�������� ������...........: ";
         
            cout << catalog[i]->product.product_name << '\n';
          
            
          
            /********** ����� ��������� **********/
         
            cout << "��������� ������.....: ";
            cout << setw(4)  << catalog[i]->product.price << " ���." << '\n';

            /********** ����� ���������� ������ ������ �� ������ **********/
        
            cout << "���������� ������ ������ �� ������...: ";
            cout << setw(4) << catalog[i]->product.quantity << " ��." << '\n';
           
            cout << '\n';
            cout << '\n';
        }
        for (int i = 0; i < size; i++)
        {
            delete catalog[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}