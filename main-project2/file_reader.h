#ifndef FILE_READER_H
#define FILE_READER_H

#include "product-catalog.h"

void read(const char* file_name, product_catalog* array[], int& size);

#endif
