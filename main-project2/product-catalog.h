#ifndef PRODUCT_CATALOG_H
#define PRODUCT_CATALOG_H

#include "constants.h"



struct product
{

    double price;
    int quantity;
    char category[MAX_STRING_SIZE];
    char product_name[MAX_STRING_SIZE];
   
};

struct product_catalog {
    product;

};


#endif
